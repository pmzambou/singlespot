package training.androidkotlin.singlespot

import android.location.Location
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.widget.Toast
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class GetPointsActivity : AppCompatActivity() {

    val listPoints = mutableListOf<Point>()
    val groupPointsList = mutableListOf<MutableList<Point>>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_get_points)

        val adapter = DataAdapter(listPoints)
        val recyclerView: RecyclerView = findViewById(R.id.recycler_view)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = adapter

        val retrofitJson = Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()

        val serviceJson: Api = retrofitJson.create(Api::class.java)

        val call: Call<List<String>> = serviceJson.allLinks

        call.enqueue(object : Callback<List<String>> {
            override fun onResponse(call: Call<List<String>>, response: Response<List<String>>) {
                val listLink = response.body()
                val service: GetPoint = retrofitJson.create(GetPoint::class.java)

                for (i in 0 until listLink!!.size) {
                    val c: Call<Point> = service.getData(listLink[i])
                    c.enqueue(object : Callback<Point> {
                        override fun onResponse(call: Call<Point>, response: Response<Point>) {
                            val point = response.body()
                            listPoints.add(point!!)
                            adapter.notifyDataSetChanged()
                        }

                        override fun onFailure(call: Call<Point>, t: Throwable) {
                            Log.i("Erreur", "impossible d'accéder à la ressource recherchée")
                        }
                    })
                }
                sortPoints()
            }

            override fun onFailure(call: Call<List<String>>, t: Throwable) {
                Log.i("Erreur", "impossible d'accéder au lien recherché")
            }

        })
    }

    fun sortPoints() {
        for (i in 0 until listPoints.size) {

            lateinit var location: Location
            location.latitude = listPoints[i].latitude
            location.longitude = listPoints[i].longitude

            if (groupPointsList.isEmpty()) {
                val newList = mutableListOf<Point>()
                newList.add(listPoints[i])
                groupPointsList.add(newList)
            } else {
                val list: MutableList<Point>? = groupPointsList.find { l: MutableList<Point> ->
                    l.first().evaluateDistance(location)
                }
                val index = groupPointsList.indexOf(list)
                if (list!!.isEmpty()) {
                    val newList = mutableListOf<Point>()
                    newList.add(listPoints[i])
                    groupPointsList.add(newList)
                } else {
                    groupPointsList[index].add(listPoints[i])
                }
            }
        }
        Toast.makeText(applicationContext, "Fin du chargement des données. ${listPoints.size} points crées", Toast.LENGTH_LONG).show()
    }
}

