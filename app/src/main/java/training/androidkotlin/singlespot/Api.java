package training.androidkotlin.singlespot;


import java.util.List;
import retrofit2.Call;
import retrofit2.http.GET;

public interface Api {
    String BASE_URL = "http://static.singlespot.com/tests/mobile5/";

    @GET("points.json")
    Call<List<String>> getAllLinks();
}