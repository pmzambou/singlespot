package training.androidkotlin.singlespot

import android.location.Location

class Point(val latitude: Double, val longitude: Double) {

    fun evaluateDistance(location: Location): Boolean {
        lateinit var l: Location
        l.latitude = this.latitude
        l.longitude = this.longitude

        return l.distanceTo(location) < 10.0F
    }
}
