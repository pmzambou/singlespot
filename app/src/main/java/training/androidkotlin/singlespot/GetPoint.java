package training.androidkotlin.singlespot;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

public interface GetPoint {
    @GET
    Call<Point> getData(@Url String url);
}
